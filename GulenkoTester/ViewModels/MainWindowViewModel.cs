﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GulenkoTester.Gulenko;
using GulenkoTester.Serializers;
using GulenkoTester.View.ViewModel;
using Microsoft.Win32;
using Prism.Mvvm;

namespace GulenkoTester.ViewModels {
    public class MainWindowViewModel : BindableBase {
        private readonly Window _parentWindow;
        private Questionnaire _questionnaire;
        private ObservableCollection<QuestionPair> _questionPairs;

        public Questionnaire CurrentQuestionnaire {
            get => _questionnaire;
            set => SetProperty(ref _questionnaire, value);
        }

        public ObservableCollection<QuestionPair> QuestionPairs {
            get => _questionPairs;
            set => SetProperty(ref _questionPairs, value);
        }

        public ICommand StartTestCommand { get; }
        public ICommand AnalizeResultsCommand { get; }
        public ICommand LoadQuestionairCommand { get; }
        public ICommand SaveQuestionairCommand { get; }
        public ICommand EditQuestionairCommand { get; }

        public MainWindowViewModel(Window parentWindow) {
            _parentWindow = parentWindow;

            string path = System.IO.Directory.GetCurrentDirectory() + @"\Questionaire.xml";
            var serailizer = new QuestionairSerailizer();
            Questionnaire questionair = serailizer.Deserialize(path);
            if (questionair != null) {
                _questionnaire = questionair;
            }
            else {
                _questionnaire = new Questionnaire();
                _questionnaire.Initialize();
            }

            _questionPairs = new ObservableCollection<QuestionPair>();

            StartTestCommand = new RelayCommand(param => StartTest());
            AnalizeResultsCommand = new RelayCommand(param => AnalizeResults());
            LoadQuestionairCommand = new RelayCommand(param => LoadQuestionair());
            SaveQuestionairCommand = new RelayCommand(param => SaveQuestionair());
            EditQuestionairCommand = new RelayCommand(param => EditQuestionair());
        }

        private void StartTest() {
            var pairs = new ObservableCollection<QuestionPair>();
            foreach (QuestionBlock questionBlock in _questionnaire.QuestionBlocks)
            foreach (QuestionPair questionPair in questionBlock.GenerateQuestionPairs())
                pairs.Add(questionPair);
            QuestionPairs = pairs;

            if (_questionPairs.Count == 0)
                MessageBox.Show(_parentWindow, "В вопроснике нет ни одного вопроса");
        }

        private void AnalizeResults() {
            int unanwseredCount = _questionPairs.Count(x => !x.IsAnwsered());
            if (unanwseredCount > 0) {
                string message = $"Ответьте на оставшиеся {unanwseredCount} вопросов";
                MessageBox.Show(_parentWindow, message);
                return;
            }

            var aspectCount = new Dictionary<TypeAspect, int>();
            var typeAspects = Enum.GetValues(typeof(TypeAspect)).Cast<TypeAspect>();
            foreach (TypeAspect aspect in typeAspects)
                aspectCount[aspect] = 0;

            foreach (QuestionPair questionPair in _questionPairs) {
                TypeAspect aspect = questionPair.GetAnwserType();
                aspectCount[aspect] = aspectCount[aspect] + 1;
            }

            TestResult result = TestAnalizer.Analize(aspectCount);
            var dialog = new ResultWindow(result) {Owner = _parentWindow};
            dialog.ShowDialog();
        }

        private void LoadQuestionair() {
            var dialog = new OpenFileDialog {Filter = "XML Files|*.xml"};
            if (dialog.ShowDialog() != true)
                return;

            var serailizer = new QuestionairSerailizer();
            Questionnaire questionair = serailizer.Deserialize(dialog.FileName);
            if (questionair != null)
                CurrentQuestionnaire = questionair;
            else
                MessageBox.Show(_parentWindow, serailizer.LastError);
        }

        private void SaveQuestionair() {
            var dialog = new SaveFileDialog() {Filter = "XML Files|*.xml"};
            if (dialog.ShowDialog() != true)
                return;

            var serailizer = new QuestionairSerailizer();
            bool success = serailizer.Serialize(_questionnaire, dialog.FileName);
            if (success)
                return;

            MessageBox.Show(_parentWindow, serailizer.LastError);
        }

        private void EditQuestionair() {
            var questionair = (Questionnaire) _questionnaire.Clone();
            var dialog = new SettingsWindow(questionair) {Owner = _parentWindow};
            if (dialog.ShowDialog() != true)
                return;

            CurrentQuestionnaire = dialog.GetQuestionnaire;
        }
    }
}
