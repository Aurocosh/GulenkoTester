﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GulenkoTester.Gulenko;
using GulenkoTester.Serializers;
using GulenkoTester.View.ViewModel;
using Microsoft.Win32;
using Prism.Mvvm;

namespace GulenkoTester.ViewModels {
    public class SettingsWindowViewModel : BindableBase {
        private readonly Window _parentWindow;
        private Questionnaire _questionnaire;
        private QuestionBlock _selectedBlock;
        private QuestionPairTemplate _selectedPairTemplate;

        public ICommand AddPairCommand { get; }
        public ICommand DeletePairCommand { get; }
        public ICommand LoadQuestionairCommand { get; }
        public ICommand SaveQuestionairCommand { get; }

        public Questionnaire CurrentQuestionnaire {
            get => _questionnaire;
            set => SetProperty(ref _questionnaire, value);
        }

        public QuestionBlock SelectedBlock {
            get => _selectedBlock;
            set => SetProperty(ref _selectedBlock, value);
        }

        public QuestionPairTemplate SelectedPairTemplate {
            get => _selectedPairTemplate;
            set => SetProperty(ref _selectedPairTemplate, value);
        }

        public SettingsWindowViewModel(Window parentWindow, Questionnaire questionnaire) {
            _parentWindow = parentWindow;
            _questionnaire = questionnaire;

            AddPairCommand = new RelayCommand(param => AddPair(), param => CanAddPair());
            DeletePairCommand = new RelayCommand(param => DeletePair(), param => CanDeletePair());
            LoadQuestionairCommand = new RelayCommand(param => LoadQuestionair());
            SaveQuestionairCommand = new RelayCommand(param => SaveQuestionair());
        }

        private void AddPair() {
            SelectedBlock.QuestionPairs.Add(new QuestionPairTemplate());
        }

        private void DeletePair() {
            SelectedBlock.QuestionPairs.Remove(SelectedPairTemplate);
        }

        private void LoadQuestionair() {
            var dialog = new OpenFileDialog {Filter = "XML Files|*.xml"};
            if (dialog.ShowDialog() != true)
                return;

            var serailizer = new QuestionairSerailizer();
            Questionnaire questionair = serailizer.Deserialize(dialog.FileName);
            if (questionair != null)
                CurrentQuestionnaire = questionair;
            else
                MessageBox.Show(_parentWindow, serailizer.LastError);
        }

        private void SaveQuestionair() {
            var dialog = new SaveFileDialog {Filter = "XML Files|*.xml"};
            if (dialog.ShowDialog() != true)
                return;

            var serailizer = new QuestionairSerailizer();
            bool success = serailizer.Serialize(_questionnaire, dialog.FileName);
            if (success)
                return;

            MessageBox.Show(_parentWindow, serailizer.LastError);
        }

        private bool CanAddPair() {
            return SelectedBlock != null;
        }

        private bool CanDeletePair() {
            return SelectedBlock != null && SelectedPairTemplate != null;
        }
    }
}
