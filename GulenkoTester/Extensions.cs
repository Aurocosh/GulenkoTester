﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace GulenkoTester {
    public static class Extensions {
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable {
            return listToClone.Select(item => (T) item.Clone()).ToList();
        }

        public static ObservableCollection<T> Clone<T>(this ObservableCollection<T> listToClone) where T : ICloneable {
            return new ObservableCollection<T>(listToClone.Select(item => (T) item.Clone()).ToList());
        }
    }
}
