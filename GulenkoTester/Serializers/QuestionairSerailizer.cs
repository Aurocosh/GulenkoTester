﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using GulenkoTester.Gulenko;

namespace GulenkoTester.Serializers {
    public class QuestionairSerailizer {
        private readonly XmlSerializer _serializer;
        public string LastError;

        public QuestionairSerailizer() {
            var types = new[] {typeof(QuestionBlock), typeof(QuestionPairTemplate)};
            _serializer = new XmlSerializer(typeof(Questionnaire), types);
            LastError = "";
        }

        public bool Serialize(Questionnaire questionnaire, string fileName) {
            try {
                using (var fileStream = new FileStream(fileName, FileMode.Create)) {
                    _serializer.Serialize(fileStream, questionnaire);
                }
                return true;
            }
            catch (Exception exception) {
                LastError = exception.ToString();
                return false;
            }
        }

        public Questionnaire Deserialize(string fileName) {
            try {
                using (var fileStream = new FileStream(fileName, FileMode.Open)) {
                    return (Questionnaire) _serializer.Deserialize(fileStream);
                }
            }
            catch (Exception exception) {
                LastError = exception.ToString();
                return null;
            }
        }
    }
}
