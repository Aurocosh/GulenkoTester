﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GulenkoTester.Gulenko;
using GulenkoTester.ViewModels;

namespace GulenkoTester
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window {
        public SettingsWindowViewModel ViewModel { get; }
        public Questionnaire GetQuestionnaire => ViewModel.CurrentQuestionnaire;

        public SettingsWindow(Questionnaire questionnaire)
        {
            InitializeComponent();
            ViewModel = new SettingsWindowViewModel(this, questionnaire);
            TopLevelDockPanel.DataContext = ViewModel;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e) {
            DialogResult = true;
        }
    }
}
