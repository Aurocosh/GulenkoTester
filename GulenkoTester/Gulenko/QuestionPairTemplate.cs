﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Prism.Mvvm;

namespace GulenkoTester.Gulenko {
    [XmlType("QuestionPairTemplate")]
    public class QuestionPairTemplate : BindableBase, ICloneable {
        private string _firstQuestion;
        private string _secondQuestion;

        [XmlAttribute("FirstQuestion")]
        public string FirstQuestion {
            get => _firstQuestion;
            set => SetProperty(ref _firstQuestion, value);
        }

        [XmlAttribute("SecondQuestion")]
        public string SecondQuestion {
            get => _secondQuestion;
            set => SetProperty(ref _secondQuestion, value);
        }

        public QuestionPairTemplate() {
            _firstQuestion = "Первый вопрос";
            _secondQuestion = "Второй вопрос";
        }

        public QuestionPairTemplate(string firstQuestion, string secondQuestion) {
            _firstQuestion = firstQuestion;
            _secondQuestion = secondQuestion;
        }

        public object Clone() {
            return new QuestionPairTemplate(_firstQuestion, _secondQuestion);
        }
    }
}
