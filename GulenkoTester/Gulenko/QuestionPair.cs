﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace GulenkoTester.Gulenko {
    public class QuestionPair : BindableBase {
        private string _firstQuestion;
        private string _secondQuestion;

        private readonly TypeAspect _firstAspect;
        private readonly TypeAspect _secondAspect;

        private QuestionAnwser _anwser;

        public string FirstQuestion {
            get => _firstQuestion;
            set => SetProperty(ref _firstQuestion, value);
        }

        public string SecondQuestion {
            get => _secondQuestion;
            set => SetProperty(ref _secondQuestion, value);
        }

        public QuestionAnwser Anwser {
            get => _anwser;
            set => SetProperty(ref _anwser, value);
        }

        public QuestionPair(string firstQuestion, string secondQuestion, TypeAspect firstAspect, TypeAspect secondAspect) {
            _firstQuestion = firstQuestion;
            _secondQuestion = secondQuestion;
            _firstAspect = firstAspect;
            _secondAspect = secondAspect;
        }

        public TypeAspect GetAnwserType() {
            return _anwser == QuestionAnwser.FirstSelected ? _firstAspect : _secondAspect;
        }

        public bool IsAnwsered() {
            return _anwser != QuestionAnwser.NotAnwsered;
        }
    }
}
