﻿namespace GulenkoTester.Gulenko {
    public enum TypeAspect {
        Extroversion,
        Introversion,
        Rationality,
        Irrationality,
        Sensorics,
        Intuition,
        Logics,
        Ethics
    }
}
