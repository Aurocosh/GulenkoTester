﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GulenkoTester.Gulenko {
    public class TestResult {
        public float Rationality;
        public float Irrationality;
        public float Logics;
        public float Ethics;
        public float Extroversion;
        public float Introversion;
        public float Sensorics;
        public float Intuition;
        public string PersonalityType;
        public string TeamRole;

        public TestResult() {
            Rationality = 0;
            Irrationality = 0;
            Logics = 0;
            Ethics = 0;
            Extroversion = 0;
            Introversion = 0;
            Sensorics = 0;
            Intuition = 0;
            PersonalityType = "";
            TeamRole = "";
        }

        public override string ToString() {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Рациональность: {Rationality:0.00} \tИррациональность: {Irrationality:0.00}");
            stringBuilder.AppendLine($"Логика: {Logics:0.00} \t\tЭтика: {Ethics:0.00}");
            stringBuilder.AppendLine($"Экстраверсия: {Extroversion:0.00} \tИнтроверсия: {Introversion:0.00}");
            stringBuilder.AppendLine($"Сенсорика: {Sensorics:0.00} \t\tИнтуиция: {Intuition:0.00}");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine($"Тип личности: {PersonalityType}");
            stringBuilder.AppendLine($"Роль в команде: {TeamRole}");
            return stringBuilder.ToString();
        }
    }
}
