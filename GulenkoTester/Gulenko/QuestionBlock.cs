﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Prism.Mvvm;

namespace GulenkoTester.Gulenko {
    [XmlType("QuestionBlock")]
    [XmlInclude(typeof(QuestionPairTemplate))]
    public class QuestionBlock : BindableBase, ICloneable {
        private string _name;
        private TypeAspect _oddAnwserType;
        private TypeAspect _evenAnwserType;
        private ObservableCollection<QuestionPairTemplate> _questionPairs;

        [XmlAttribute("Name")]
        public string Name {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        [XmlAttribute("OddAnwserType")]
        public TypeAspect OddAnwserType {
            get => _oddAnwserType;
            set => SetProperty(ref _oddAnwserType, value);
        }

        [XmlAttribute("EvenAnwserType")]
        public TypeAspect EvenAnwserType {
            get => _evenAnwserType;
            set => SetProperty(ref _evenAnwserType, value);
        }

        [XmlArray("QuestionPairs")]
        [XmlArrayItem("Pair")]
        public ObservableCollection<QuestionPairTemplate> QuestionPairs {
            get => _questionPairs;
            set => SetProperty(ref _questionPairs, value);
        }

        private QuestionBlock() {
            _name = "Экстраверсия - Интроверсия";
            _oddAnwserType = TypeAspect.Extroversion;
            _evenAnwserType = TypeAspect.Introversion;
            _questionPairs = new ObservableCollection<QuestionPairTemplate>();
        }

        public QuestionBlock(string name, TypeAspect oddAnwserType, TypeAspect evenAnwserType) {
            _name = name;
            _oddAnwserType = oddAnwserType;
            _evenAnwserType = evenAnwserType;
            _questionPairs = new ObservableCollection<QuestionPairTemplate>();
        }

        private QuestionBlock(string name, TypeAspect oddAnwserType, TypeAspect evenAnwserType,
            ObservableCollection<QuestionPairTemplate> questionPairs) {
            _name = name;
            _oddAnwserType = oddAnwserType;
            _evenAnwserType = evenAnwserType;
            _questionPairs = questionPairs;
        }

        public IEnumerable<QuestionPair> GenerateQuestionPairs() {
            return _questionPairs.Select(
                x => new QuestionPair(x.FirstQuestion, x.SecondQuestion, _oddAnwserType, _evenAnwserType));
        }

        public object Clone() {
            ObservableCollection<QuestionPairTemplate> pairs = _questionPairs.Clone();
            return new QuestionBlock(_name, _oddAnwserType, _evenAnwserType, pairs);
        }
    }
}
