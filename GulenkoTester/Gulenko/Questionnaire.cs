﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Prism.Mvvm;

namespace GulenkoTester.Gulenko {
    [XmlRoot("Questionaire")]
    [XmlInclude(typeof(QuestionBlock))]
    public class Questionnaire : BindableBase, ICloneable {
        private ObservableCollection<QuestionBlock> _questionBlocks;

        [XmlArray("QuestionBlocks")]
        [XmlArrayItem("Block")]
        public ObservableCollection<QuestionBlock> QuestionBlocks {
            get => _questionBlocks;
            set => SetProperty(ref _questionBlocks, value);
        }

        public Questionnaire() {
            _questionBlocks = new ObservableCollection<QuestionBlock>();
        }

        private Questionnaire(ObservableCollection<QuestionBlock> questionBlocks) {
            _questionBlocks = questionBlocks;
        }

        public void Initialize() {
            _questionBlocks = new ObservableCollection<QuestionBlock> {
                new QuestionBlock("Экстраверсия - Интроверсия", TypeAspect.Extroversion,
                    TypeAspect.Introversion),
                new QuestionBlock("Рациональность - Иррациональность", TypeAspect.Rationality,
                    TypeAspect.Irrationality),
                new QuestionBlock("Сенсорика - Интуиция", TypeAspect.Sensorics,
                    TypeAspect.Intuition),
                new QuestionBlock("Логика - Этика", TypeAspect.Logics, TypeAspect.Ethics)
            };
        }

        public object Clone() {
            ObservableCollection<QuestionBlock> blocks = _questionBlocks.Clone();
            return new Questionnaire(blocks);
        }
    }
}
