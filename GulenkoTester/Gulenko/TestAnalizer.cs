﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GulenkoTester.Gulenko
{
    public static class TestAnalizer {
        private struct ResultKey {
            public TypeAspect RatIrratKey;
            public TypeAspect LogEthiKey;
            public TypeAspect ExtaIntroKey;
            public TypeAspect SenIntuKey;

            public ResultKey(TypeAspect ratIrratKey, TypeAspect logEthiKey, TypeAspect extaIntroKey, TypeAspect senIntuKey) {
                RatIrratKey = ratIrratKey;
                LogEthiKey = logEthiKey;
                ExtaIntroKey = extaIntroKey;
                SenIntuKey = senIntuKey;
            }
        }

        private class PersonRoles {
            public readonly string PersonalityType;
            public readonly string TeamRole;

            public PersonRoles(string personalityType, string teamRole) {
                PersonalityType = personalityType;
                TeamRole = teamRole;
            }
        }

        private static readonly Dictionary<ResultKey, PersonRoles> RolesDictionary = new Dictionary<ResultKey, PersonRoles> {
            {new ResultKey(TypeAspect.Rationality, TypeAspect.Logics, TypeAspect.Extroversion, TypeAspect.Sensorics),new PersonRoles("Управитель","Двигатель группы")},
            {new ResultKey(TypeAspect.Rationality, TypeAspect.Logics, TypeAspect.Extroversion, TypeAspect.Intuition),new PersonRoles("Предприниматель","Двигатель группы")},

            {new ResultKey(TypeAspect.Rationality, TypeAspect.Logics, TypeAspect.Introversion, TypeAspect.Sensorics),new PersonRoles("Инспектор","Систематизатор")},
            {new ResultKey(TypeAspect.Rationality, TypeAspect.Logics, TypeAspect.Introversion, TypeAspect.Intuition),new PersonRoles("Аналитик","Систематизатор")},

            {new ResultKey(TypeAspect.Rationality, TypeAspect.Ethics, TypeAspect.Extroversion, TypeAspect.Sensorics),new PersonRoles("Энтузиаст","Эмоциональный вовлекатель")},
            {new ResultKey(TypeAspect.Rationality, TypeAspect.Ethics, TypeAspect.Extroversion, TypeAspect.Intuition),new PersonRoles("Наставник","Эмоциональный вовлекатель")},

            {new ResultKey(TypeAspect.Rationality, TypeAspect.Ethics, TypeAspect.Introversion, TypeAspect.Sensorics),new PersonRoles("Хранитель","Гармонизатор")},
            {new ResultKey(TypeAspect.Rationality, TypeAspect.Ethics, TypeAspect.Introversion, TypeAspect.Intuition),new PersonRoles("Гуманист","Гармонизатор")},


            {new ResultKey(TypeAspect.Irrationality, TypeAspect.Logics, TypeAspect.Extroversion, TypeAspect.Sensorics),new PersonRoles("Маршал","Неформальный лидер")},
            {new ResultKey(TypeAspect.Irrationality, TypeAspect.Logics, TypeAspect.Extroversion, TypeAspect.Intuition),new PersonRoles("Искатель","Генератор идей")},

            {new ResultKey(TypeAspect.Irrationality, TypeAspect.Logics, TypeAspect.Introversion, TypeAspect.Sensorics),new PersonRoles("Критик","Отражатель")},
            {new ResultKey(TypeAspect.Irrationality, TypeAspect.Logics, TypeAspect.Introversion, TypeAspect.Intuition),new PersonRoles("Мастер","Доводчик")},

            {new ResultKey(TypeAspect.Irrationality, TypeAspect.Ethics, TypeAspect.Extroversion, TypeAspect.Sensorics),new PersonRoles("Политик","Неформальный лидер")},
            {new ResultKey(TypeAspect.Irrationality, TypeAspect.Ethics, TypeAspect.Extroversion, TypeAspect.Intuition),new PersonRoles("Советчик","Генератор идей")},

            {new ResultKey(TypeAspect.Irrationality, TypeAspect.Ethics, TypeAspect.Introversion, TypeAspect.Sensorics),new PersonRoles("Лирик","Отражатель")},
            {new ResultKey(TypeAspect.Irrationality, TypeAspect.Ethics, TypeAspect.Introversion, TypeAspect.Intuition),new PersonRoles("Посредник","Доводчик")},
        };

        public static TestResult Analize(Dictionary<TypeAspect, int> aspectCount) {
            var result = new TestResult();

            int rationality = aspectCount[TypeAspect.Rationality];
            int irrationality = aspectCount[TypeAspect.Irrationality];

            int ratIrratSum = rationality + irrationality;
            if (ratIrratSum > 0) {
                result.Rationality = rationality / (float) ratIrratSum;
                result.Irrationality = irrationality / (float) ratIrratSum;
            }

            int logics = aspectCount[TypeAspect.Logics];
            int ethics = aspectCount[TypeAspect.Ethics];

            int logEthiSum = logics + ethics;
            if (logEthiSum > 0) {
                result.Logics = logics / (float) logEthiSum;
                result.Ethics = ethics / (float) logEthiSum;
            }

            int extroversion = aspectCount[TypeAspect.Extroversion];
            int introversion = aspectCount[TypeAspect.Introversion];

            int extraIntroSum = extroversion + introversion;
            if (extraIntroSum > 0) {
                result.Extroversion = extroversion / (float) extraIntroSum;
                result.Introversion = introversion / (float) extraIntroSum;
            }

            int sensorics = aspectCount[TypeAspect.Sensorics];
            int intuition = aspectCount[TypeAspect.Intuition];

            int senIntuSum = sensorics + intuition;
            if (senIntuSum > 0) {
                result.Sensorics = sensorics / (float) senIntuSum;
                result.Intuition = intuition / (float) senIntuSum;
            }

            var resultKey = new ResultKey {
                RatIrratKey = result.Rationality >= 0.5 ? TypeAspect.Rationality : TypeAspect.Irrationality,
                LogEthiKey = result.Logics >= 0.5 ? TypeAspect.Logics : TypeAspect.Ethics,
                ExtaIntroKey = result.Extroversion >= 0.5 ? TypeAspect.Extroversion : TypeAspect.Introversion,
                SenIntuKey = result.Sensorics >= 0.5 ? TypeAspect.Sensorics : TypeAspect.Intuition
            };
            PersonRoles roles = RolesDictionary[resultKey];

            result.PersonalityType = roles.PersonalityType;
            result.TeamRole = roles.TeamRole;

            return result;
        }
    }
}
